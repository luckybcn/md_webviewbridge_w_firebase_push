package com.meetingdoctors.samplefirebase.webview.backup;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.meetingdoctors.samplefirebase.webview.backup.WebViewUtilsBACKUP;

public class WebViewJsInterfaceBACKUP {

  private WebView webView;
  private Context context;

  public WebViewJsInterfaceBACKUP(Context context, WebView webView) {
    this.webView = webView;
    this.context=context;
  }

  @JavascriptInterface
  public void handleMessage(String message) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
  }

  @JavascriptInterface
  public void setWebViewTextCallback(){
    String script = WebViewUtilsBACKUP.formatScript("setText","This is a text from Android which is set " +
        "in the html page.");
    WebViewUtilsBACKUP.callJavaScriptFunction(webView,script);
  }
}
