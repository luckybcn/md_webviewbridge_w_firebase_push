# WebViewDemo

A demo project for showing how to use WebViews in Android with Fire base. Also you can check how native App can communicate with JavaScript content.


## Things covered in this project

* Load a local html file into a WebView with the basic WebView settings.
* Handle Javascript callbacks from the Javascript to the client.
* Make Javascript callbacks from the client to Javascript.
* Set WebViewClient to the WebView
* Debugging of WebView
* Firebase Cloud Messaging
* Firebase Configuration & Implementation

 <br/>

![MainActivity](images/mainactivity.jpg) ![Widget Web](images/widgetwebactivity.jpg) ![JS Interface sample #1](images/jsactivity_js_toast.jpg) ![JS Interface sample #2](images/jsactivity_token_from_js_toast.jpg)

## Key Concepts

This demo/spike purpose is to show how native Android Sdk can set bi-directional communication with JavaScript code embedded within a webview via **_@JavaScriptInterface_**. To achieve this goal, Android lets
create a Java/Android wrapper class containing all actions needed to interact with _JS_ embedded inside webview to client App, and inverse.

Here is a Sample:

```kotlin
 class WebViewJsInterface(private val context: Context, private val webView: WebView) {

    @JavascriptInterface
    fun handleMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @JavascriptInterface
    fun setWebViewTextCallback() {
        val script = WebViewUtils.formatScript("setText", "This is a text from Android which is set " + "in the html page.")
        WebViewUtils.callJavaScriptFunction(webView, script)
    }

    @JavascriptInterface
    fun getPushToken() {
        val script = WebViewUtils.formatScript("action", Repository.getPushToken(context) as String)
        WebViewUtils.callJavaScriptFunction(webView, script)
    }
}
```

Your class will need to reference to this interface for you webview, just like this:

```kotlin
   val webSettings = widget_webview.settings
   webSettings.javaScriptEnabled = true
   widget_webview.addJavascriptInterface(WebViewJsInterface(this, widget_webview), "AndroidNativeInterface")
```

On the ohter hand, your JS code will need to communicate with your native JS Interface, in this case, for the sake of simplicity, just add this to your Html code:


```javascript
<!DOCTYPE html>
<html>
<style>
</style>

<body>
  <div id="bottom-container">
    <button type="button" onclick="showToast()" class="button-class">Show Toast Message
    </button>
    <button type="button" class="button-class" onclick="getPushTokenFromClient()">Show me the Token!!
    </button>
</div>

<script>
    function showToast(){
        AndroidNativeInterface.handleMessage('This is a toast message from Javascript !!');
    }

    function showCustomToast(text){
        AndroidNativeInterface.handleMessage(text);
    }

    function giveCallbackToClient(){
        AndroidNativeInterface.setWebViewTextCallback();
    }

    function setText(text){
        document.getElementById('text-container').innerHTML=text;
    }

    function getPushTokenFromClient() {
        AndroidNativeInterface.getPushToken()
    }

    function storePushToken(token) {
        showCustomToast(token)
       // Do your stuff here
    }
</script>
</body>
</html>
```

## ADITTIONAL INFO

There is available a complete guide for FCM(FireBase Cloud Messaging)  configuration & implementation inside's project, just check **_./docs_** to check a pdf file with complete guide 

![FCM pdf guide](images/pdf_fcm_location.png)