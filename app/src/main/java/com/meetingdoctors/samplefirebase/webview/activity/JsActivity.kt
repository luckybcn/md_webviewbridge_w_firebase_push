package com.meetingdoctors.samplefirebase.webview.activity

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.meetingdoctors.samplefirebase.webview.R
import com.meetingdoctors.samplefirebase.webview.jsutil.CustomWebViewClient
import com.meetingdoctors.samplefirebase.webview.jsutil.WebViewJsInterface

class JsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        initWebView()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.settings.useWideViewPort = false
        webView.addJavascriptInterface(WebViewJsInterface(this, webView), "AndroidNativeInterface")
        webView.webViewClient = CustomWebViewClient()
        webView.loadUrl("file:///android_asset/main.html")
    }
}