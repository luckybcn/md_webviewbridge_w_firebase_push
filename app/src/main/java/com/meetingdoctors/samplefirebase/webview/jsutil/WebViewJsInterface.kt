package com.meetingdoctors.samplefirebase.webview.jsutil

import android.content.Context
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.Toast
import com.meetingdoctors.samplefirebase.webview.data.Repository

class WebViewJsInterface(private val context: Context, private val webView: WebView) {

    @JavascriptInterface
    fun handleMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @JavascriptInterface
    fun setWebViewTextCallback() {
        val script = WebViewUtils.formatScript("setText", "This is a text from Android which is set " + "in the html page.")
        WebViewUtils.callJavaScriptFunction(webView, script)
    }

    @JavascriptInterface
    fun getPushToken() {
        val script = WebViewUtils.formatScript("storePushToken", Repository.getPushToken(context) as String)
        WebViewUtils.callJavaScriptFunction(webView, script)
    }
}