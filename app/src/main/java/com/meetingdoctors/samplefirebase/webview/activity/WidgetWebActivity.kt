package com.meetingdoctors.samplefirebase.webview.activity

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.meetingdoctors.samplefirebase.webview.R
import com.meetingdoctors.samplefirebase.webview.jsutil.CustomWebChromeClient
import com.meetingdoctors.samplefirebase.webview.jsutil.CustomWebViewClient
import com.meetingdoctors.samplefirebase.webview.jsutil.WebViewJsInterface
import kotlinx.android.synthetic.main.widget_activity_layout.*

internal class WidgetWebActivity : AppCompatActivity() {

    private var url: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.widget_activity_layout)

        val intent = intent
        if (intent != null) {
            val urlPassed = intent.getStringExtra(URL_KEY)
            url = urlPassed ?: ""
        }

        val webSettings = widget_webview.settings
        webSettings.javaScriptEnabled = true
        widget_webview.settings.domStorageEnabled = true
        widget_webview.settings.javaScriptCanOpenWindowsAutomatically = true
        widget_webview.isVerticalScrollBarEnabled = false
        widget_webview.isHorizontalScrollBarEnabled = false
        widget_webview.addJavascriptInterface(WebViewJsInterface(this, widget_webview), "AndroidNativeInterface")
        widget_webview.webViewClient = CustomWebViewClient()
        //if your build is in debug mode, enable webviews inspection
        widget_webview.webChromeClient = CustomWebChromeClient()
        widget_webview.loadUrl(url)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true)
        }
    }

    override fun onBackPressed() {
        if (widget_webview.canGoBack()) {
            widget_webview.goBack()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        const val URL_KEY = "urlKey"
    }
}

