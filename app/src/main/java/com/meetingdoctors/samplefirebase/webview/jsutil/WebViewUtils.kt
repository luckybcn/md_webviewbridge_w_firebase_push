package com.meetingdoctors.samplefirebase.webview.jsutil

import android.os.Build
import android.webkit.WebView

object WebViewUtils {

    fun callJavaScriptFunction(webView: WebView?, script: String) {
        if (webView == null) {
            return
        }
        webView.post {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript(script) { }
            } else {
                //  String script = displayAlert('Hello World' , true);
                webView.loadUrl("javascript:$script")
            }
        }
    }

    fun formatScript(function: String,
                     vararg params: Any): String {

        val builder = StringBuilder(function).append('(')
        val length = params.size
        for (i in params.indices) {
            if (params[i] is String) {
                builder.append("\'")
            }
            builder.append(params[i])
            if (params[i] is String) {
                builder.append("\'")
            }
            if (i != length - 1) {
                builder.append(",")
            }
        }

        builder.append(')')
        return builder.toString()
    }
}
