package com.meetingdoctors.samplefirebase.webview.jsutil

import android.util.Log
import android.webkit.ConsoleMessage
import android.webkit.WebChromeClient

class CustomWebChromeClient: WebChromeClient() {

    override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
        Log.i("WebChromeClient", "${consoleMessage!!.messageLevel()}\nMessage: ${consoleMessage?.message()}\nLine : ${consoleMessage?.lineNumber()} \nSourceId: ${consoleMessage?.sourceId()}")
        return super.onConsoleMessage(consoleMessage)
    }
}