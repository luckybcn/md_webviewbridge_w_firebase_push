package com.meetingdoctors.samplefirebase.webview.data

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.meetingdoctors.samplefirebase.webview.helper.SharedPreferencesHelper
import com.meetingdoctors.samplefirebase.webview.helper.SharedPreferencesHelper.getDouble

@SuppressLint("StaticFieldLeak")
object Repository {
    private lateinit var context: Context

    fun getCurrentContext(): Context {
        return context
    }

    fun setContext(context: Context) {
        this.context = context
    }

    fun setPushToken(context: Context, pushToken: String?) {
        if (getPushToken(context) == null || getPushToken(context) != pushToken) {
            SharedPreferencesHelper.put(context, "pushToken", pushToken ?: "no_token")
            SharedPreferencesHelper.put(context, "pushTokenRegistered", 0.0)
        }

        registerPushToken(context)
    }

    fun getPushToken(context: Context): String? {
        return SharedPreferencesHelper.getString(context, "pushToken")
    }

    private fun registerPushToken(context: Context) {
        Log.i("GCM", "registerPushToken")
        if (getDouble(context, "pushTokenRegistered") != 1.0) {
            SharedPreferencesHelper.put(context, "pushTokenRegistered", 1.0)
        }
    }
}