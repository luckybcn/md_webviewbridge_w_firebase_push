package com.meetingdoctors.samplefirebase.webview

import android.app.Application
import android.content.Context
import android.util.Log
import com.meetingdoctors.samplefirebase.webview.data.Repository

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.d("App", "Application OnCreate()")
        Repository.setContext(this)
    }

    fun getContext(): Context {
        return this
    }
}