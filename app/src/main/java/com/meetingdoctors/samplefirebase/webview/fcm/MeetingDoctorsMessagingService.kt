package com.meetingdoctors.samplefirebase.webview.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.meetingdoctors.samplefirebase.webview.R
import com.meetingdoctors.samplefirebase.webview.activity.JsActivity
import com.meetingdoctors.samplefirebase.webview.data.Repository

@Suppress("DEPRECATION")
internal class MeetingDoctorsMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        Log.i("FCM", "OnMessageReceived! : ${p0.data}")

        try {
            val jsonObject = JsonObject() // com.google.gson.JsonObject
            val jsonParser = JsonParser() // com.google.gson.JsonParser
            val map = p0.data
            var data: String?

            for (key in map.keys) {
                data = map[key]
                try {
                    jsonObject.add(key, jsonParser.parse(data))
                } catch (e: Exception) {
                    jsonObject.addProperty(key, data)
                }
            }

            showPendingMessagesNotification(this, (jsonObject.get("data") as JsonObject)
                    .getAsJsonPrimitive("message").asString)
        } catch (e: Exception) {
            showPendingMessagesNotification(this)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.i("GCM", "onTokenRefresh")
        storePushToken()
    }

    companion object {

        fun storePushToken() {
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    val token = task.result?.token
                    Log.d("FCM", "FirebaseInstanceId.getInstanceId() token[$token]")
                    if (token != null) {
                        Repository.setPushToken(Repository.getCurrentContext(), token)
                    }
                } else {
                    Log.d("FCM", "FirebaseInstanceId.getInstanceId() error")
                }
            }
        }

        fun showPendingMessagesNotification(context: Context, message: String? = context.getString(R.string.unread_messages)) {
            var notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // create channel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val mChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        context.getString(R.string.push_channel_name), NotificationManager.IMPORTANCE_HIGH)
                notificationManager.createNotificationChannel(mChannel)
            }

            // build notification
            val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setColor(context.resources.getColor(R.color.colorPrimary))
                    .setContentTitle(context.resources.getString(R.string.app_name))
                    .setContentText(message
                            ?: context.resources.getString(R.string.unread_messages))
                    .setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(R.string.unread_messages)))
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_SOUND or NotificationCompat.DEFAULT_VIBRATE)

            // set the action to take when a user taps the notification
            val resultIntent = Intent(context, JsActivity::class.java)
            resultIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            val resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            notificationBuilder.setContentIntent(resultPendingIntent)

            // show the notification
            notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(NOTIFICATION_TYPE_PENDING_MESSAGES, notificationBuilder.build())
        }
    }
}

private const val NOTIFICATION_TYPE_PENDING_MESSAGES = 1
private const val NOTIFICATION_CHANNEL_ID = "sampleFirebase"