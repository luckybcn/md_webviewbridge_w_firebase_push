package com.meetingdoctors.samplefirebase.webview.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.meetingdoctors.samplefirebase.webview.R
import kotlinx.android.synthetic.main.main_activity_layout.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)

        widget_activity_button?.setOnClickListener {
            launchWidgetWeb(this, "https://sdk.dev.meetingdoctors.com/#chat/list")
        }

        jscontrols_activity_button.setOnClickListener {
            launchJsControls(this)
        }
    }

    private fun launchWidgetWeb(context: Context, url: String) {
        val intent = Intent(context, WidgetWebActivity::class.java)
        intent.putExtra(WidgetWebActivity.URL_KEY, url)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)

        if (context is Activity) {
            context.overridePendingTransition(0, 0)
        }
    }

    private fun launchJsControls(context: Context) {
        val intent = Intent(context, JsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)

        if (context is Activity) {
            context.overridePendingTransition(0, 0)
        }
    }
}


