package com.meetingdoctors.samplefirebase.webview.helper

import android.content.Context

object SharedPreferencesHelper {

    fun put(context: Context, name: String, value: String) {
        val editor = context.getSharedPreferences("global", Context.MODE_PRIVATE).edit()
        editor.putString(name, value)
        editor.commit()
    }

    fun put(context: Context, name: String, value: Double) {
        val editor = context.getSharedPreferences("global", Context.MODE_PRIVATE).edit()
        editor.putFloat(name, value.toFloat())
        editor.commit()
    }

    fun put(context: Context, name: String, value: Long?) {
        val editor = context.getSharedPreferences("global", Context.MODE_PRIVATE).edit()
        editor.putLong(name, value!!)
        editor.commit()
    }

    fun getString(context: Context, name: String): String? {
        val sharedPreferences = context.getSharedPreferences("global", Context.MODE_PRIVATE)
        return sharedPreferences.getString(name, null)
    }

    fun getDouble(context: Context, name: String): Double {
        val sharedPreferences = context.getSharedPreferences("global", Context.MODE_PRIVATE)
        return sharedPreferences.getFloat(name, 0f).toDouble()
    }

    fun getLong(context: Context, name: String): Long {
        val sharedPreferences = context.getSharedPreferences("global", Context.MODE_PRIVATE)
        return sharedPreferences.getLong(name, 0L)
    }

    fun getLong(context: Context, name: String, defaultValue: Long): Long {
        val sharedPreferences = context.getSharedPreferences("global", Context.MODE_PRIVATE)
        return sharedPreferences.getLong(name, defaultValue)
    }

    fun clear(context: Context) {
        val editor = context.getSharedPreferences("global", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }
}